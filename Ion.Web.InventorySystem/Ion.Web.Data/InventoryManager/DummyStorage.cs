﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ion.Web.Data.InventoryModels;

namespace Ion.Web.Data.InventoryManager
{
    public class DummyStorage : IDataStorage
    {
        List<Component> allComponents = new List<Component>();
       
        public void AddComonent(Component component)
        {
            allComponents.Add(component);

        }

        public Component[] GetComponents()
        {
            return allComponents.ToArray();
        }
    }
}
