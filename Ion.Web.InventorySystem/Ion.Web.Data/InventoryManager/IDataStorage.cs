﻿using Ion.Web.Data.InventoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Web.Data.InventoryManager
{
    public interface IDataStorage
    {
         Component[] GetComponents();
        void AddComonent(Component component);
    }
}
