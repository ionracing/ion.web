﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Web.Data.InventoryModels
{
    public class Component
    {
        public int ID { get; set; }
        public string Description { get; set;}
        public string MPN { get; set; }
        public string Manufacture { get; set; }
        public int ComponentTypeID { get; set; }
        public ComponentType Type { get; set; }
        public string TypeName { get; set; }
    }

    public class ComponentType
    {
        public int ID { get; set; }
        public string TypeName { get; set; }
    }

    public class ComponentCount
    {
        public int ID { get; set; }
        public int ComponentID { get; set; }
        public Component Component { get; set; }
        public DateTime LastUpdated { get; set; }
        public int Count { get; set; }
        public int OrderdCount { get; set; }
    }

    public class ComponentTrasaction
    {
        public int ID { get; set; }
        public int ComponentID { get; set; }
        public Component Component { get; set; }
        public int Count { get; set; }
        public DateTime Date { get; set; }
    }

    public class ManufactureInfo
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
    }
}
