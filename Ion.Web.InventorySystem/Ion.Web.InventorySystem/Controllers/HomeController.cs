﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ion.Web.Data.InventoryModels;

namespace Ion.Web.InventorySystem.Controllers
{
    public class HomeController : Controller
    {

        public string Index()
        {
            return "<h1>Hello World</h1>";
        }

        [HttpGet]
        public ActionResult JsonTest()
        {
            
            object testObj = new { FirstName = "Per", LastName = "Pettersen" };

            return Json(testObj);
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            JsonResult result = filterContext.Result as JsonResult;
            if (result!= null)
                result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            base.OnResultExecuting(filterContext);
        }

        public ActionResult IndexOld()
        {
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }

    public static class Exstension
    {
        public static void AddDefault(this Component comp)
        {
            comp.ID = 5;
        }
    }
}