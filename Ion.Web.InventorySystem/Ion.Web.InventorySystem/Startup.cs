﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ion.Web.InventorySystem.Startup))]
namespace Ion.Web.InventorySystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
